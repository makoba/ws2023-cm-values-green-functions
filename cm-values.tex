\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{CM values of automorphic Green functions}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I am giving in the Oberseminar on \emph{the Gross-Zagier formula via Borcherds' lifts} taking place in the winter term 2023/2024 in Bielefeld.
    The main reference for this talk is \cite[Section 4]{bruinier-yang}.

    I thank the organizer of the seminar, Paul Kiefer, and the previous speakers, Gabriele Bogo, Sören Sprehe, Simon Paege, Rebekka Strathausen and Annika Burmester.
    Also, use these notes at your own risk, they probably contain mistakes (feel free to tell me about these mistakes)!

    \section{Some recollections and notation}

    \subsection{The upper half plane}

    \begin{itemize}
        \item
        $\HH \subseteq \CC$ denotes the \emph{complex upper half plane}.

        \item
        We write $\tau = u + iv \in \HH$ for a typical element and set $q \coloneqq \exp(2 \pi i \tau)$.

        \item
        \[
            \calF \coloneqq \set[\big]{\tau \in \HH}{\text{$\abs{u} \leq 1/2$ and $\abs{\tau} \geq 1$}} \subseteq \HH
        \]
        denotes the \emph{standard fundamental domain}.

        \item
        For $T \in \RR_{> 0}$ we set
        \[
            \calF_T \coloneqq \set[\big]{\tau \in \calF}{\text{$\abs{v} \leq T$}}.
        \]

        \item
        \[
            \dd \mu(\tau) \coloneqq \frac{1}{v^2} \, \dd u \wedge \dd v = \frac{1}{2 i v^2} \, \dd \overline{\tau} \wedge \dd \tau
        \]
        denotes the \emph{standard invariant volume form on $\HH$}.
    \end{itemize}

    \subsection{Quadratic spaces}

    Let $(V, Q)$ be a quadratic space over $\QQ$ and let $L \subseteq V$ be an even lattice.

    \begin{itemize}
        \item
        $L' \subseteq V$ denotes the dual lattice of $L$.

        \item
        $\calS(V(\finadeles))$ denotes the $\CC$-vector space of Schwartz functions on $V(\finadeles)$.

        \item
        For $\mu \in L'/L$ we write
        \[
            \phi_{\mu} \coloneqq 1_{\mu + L(\Zhat)} \in \calS(V(\finadeles)).
        \]

        \item
        We set
        \[
            \calS_L \coloneqq \bigoplus_{\mu \in L'/L} \CC \cdot \phi_{\mu} \subseteq \calS(V(\finadeles))
        \]
        and denote by $\anglebr{\blank, \blank}$ the standard $\CC$-bilinear form on $\calS_L$.
    \end{itemize}

    \subsection{\texorpdfstring{Quadratic spaces of signature $(n, 2)$}{Quadratic spaces of signature (n, 2)}}

    Let $n \in \ZZ_{> 0}$, let $(V, Q)$ be a quadratic space over $\QQ$ of signature $(n, 2)$ and let $L \subseteq V$ be an even lattice.

    \begin{itemize}
        \item
        We let $\DD$ be the set of oriented negative definite $2$-dimensional subspaces $z$ of $V(\RR)$.
        Given such $z$ with underlying non-oriented subspace $U \subseteq V(\RR)$ the orientation determines an isomorphism of $\RR$-algebras
        \[
            \Cl^+(U) \cong \CC
        \]
        and thus an isomorphism $\GSpin(U) \cong \Res_{\CC/\RR} \bfG_m$.
        In this way we can identify $\DD$ with precisely one $\GSpin(V)(\RR)$-conjugacy class of homomorphisms $\Res_{\CC/\RR} \bfG_m \to \GSpin(V)_{\RR}$.

        The tuple $(\GSpin(V), \DD)$ is a Shimura datum and in particular $\DD$ naturally carries the structure of a finite disjoint union of Hermitian symmetric domains (in fact it has precisely two connected components).

        \item
        For $\tau \in \HH$, $z \in \DD$ and $h \in \GSpin(V)(\finadeles)$ we set
        \[
            \theta_L(\tau, z, h) \coloneqq v \sum_{\mu \in L'/L} \sum_{x \in V} \exp \roundbr[\Big]{2 \pi i \roundbr[\big]{Q(x_{z^{\perp}}) \tau + Q(x_z) \overline{\tau}}} \phi_{\mu}(h^{-1} x) \phi_{\mu}.
        \]
        Note that this is a slight generalization of the Siegel theta function $\Theta_L(\tau, z)$ that we saw in Annika's talk. In fact we have $\Theta_L(\tau, z) = \theta_L(\tau, z, 1)$.
    \end{itemize}

    \section{Setup}

    We fix the following data.
    \begin{itemize}
        \item
        A positive integer $n$, a quadratic space $(V, Q)$ over $\QQ$ of signature $(n, 2)$ and an even lattice $L \subseteq V$.
        We write $H \coloneqq \GSpin(V)$ and $\DD$ as above.

        \item
        A compact open subgroup $K \subseteq H(\finadeles)$.
        We assume that $K$ preserves $L(\Zhat) \subseteq V(\finadeles)$ and acts trivially on $L'/L \cong L'(\Zhat)/L(\Zhat)$.
        We write $X_K$ for the associated Shimura variety, so that we have
        \[
            X_K(\CC) = H(\QQ) \backslash \roundbr[\big]{\DD \times H(\finadeles)/K}.
        \]
    \end{itemize}

    \section{Automorphic Green functions}
    
    Recall that in Rebekka's talk we defined the notion of a harmonic weak Maass form.
    We write $H_{1 - n/2, \overline{\rho}_L}$ for the space that was denoted by $H^+_{1 - n/2, L^-}$ in that talk.
    
    We fix a harmonic weak Maass form $f \in H_{1 - n/2, \overline{\rho}_L}$ such that $c^+_f(m, \mu) \in \ZZ$ for all $m \in \QQ_{\leq 0}$ and $\mu \in L'/L$.

    \begin{definition}
        We define the \emph{automorphic Green function} $\Phi(z, h, f)$ on $X_K$ as the regularized theta integral
        \[
            \Phi(z, h, f) \coloneqq \int_{\calF}^{\reg} \anglebr[\big]{f(\tau), \theta_L(\tau, z, h)} \, \dd \mu(\tau).
        \]
        Here, the integral is regularized as in Annika's talk, i.e.\ it is defined as the constant term at $s = 0$ of the meromorphic function on $\CC$ defined by
        \[
            \lim_{T \to \infty} \int_{\calF_T} \anglebr[\big]{f(\tau), \theta_L(\tau, z, h)} v^{-s} \, \dd \mu(\tau)
        \]
        for $s \in \CC$ with $\re(s) \gg 0$.
    \end{definition}

    \begin{theorem}[{\cite[Theorem 4.2]{bruinier-yang}}, Annika's talk]
        The function $\Phi(z, h, f)$ has the following properties.

        \begin{itemize}
            \item
            It is smooth on $X_K \setminus Z(f)$ (where $Z(f) \in \Div(X_K)$ is a certain Heegner divisor associated to $f$).

            \item
            It has a logarithmic singularity along the divisor $- 2 Z(f)$.

            \item
            The $(1, 1)$-form $\dd \dd^c \Phi(z, h f)$ extends to a smooth form on $X_K$ and satisfies a Green current equation
            \[
                \dd \dd^c \squarebr[\big]{\Phi(z, h, f)} + \delta_{Z(f)} = \squarebr[\big]{\dd \dd^c \Phi(z, h, f)}.
            \]

            \item
            We have
            \[
                \Delta_z \Phi(z, h, f) = \frac{n}{4} \cdot c^+_f(0, 0)
            \]
            (where $\Delta_z$ is the invariant Laplace operator on $\DD$).
        \end{itemize}
    \end{theorem}

    \section{CM values of automorphic Green functions}

    We now also fix a negative definite subspace $U \subseteq V$ of dimension $2$ and introduce the following notation.
    \begin{itemize}
        \item
        $\curlybr{z_U^{\pm}} \subseteq \DD$ denotes the two point subset given by $U(\RR)$ with the two possible choices of orientation.
        
        \item
        $V_+ \subseteq V$ denotes the orthogonal complement of $U$.

        \item
        We set $T \coloneqq \GSpin(U) \subseteq H$ and $K_T \coloneqq K \cap T(\finadeles)$.

        \item
        We normalize Haar measures on $\SO(U)(\finadeles)$ and $T(\finadeles)$ so that we have
        \[
            \vol \roundbr[\big]{\SO(U)(\QQ) \backslash \SO(U)(\finadeles)} = 2 \quad \text{and} \quad \vol \roundbr[\big]{T(\QQ) \backslash T(\finadeles)} = 1.
        \]

        \item
        We set $P \coloneqq L \cap V_+$ and $N \coloneqq L \cap U$.
        For simplicity we assume that $L = P \oplus N$.
    \end{itemize}

    \begin{definition}
        We define the CM cycle $Z(U)$ on $X_K$ by
        \[
            Z(U) \coloneqq \frac{2}{w_{K, T}} \sum_{? \in \curlybr{\pm}} \sum_{h \in T(\QQ) \backslash T(\finadeles) / K_T} \squarebr[\big]{z_U^{?}, h} \in Z_0(X_K).
        \]
    \end{definition}

    \begin{goal}
        Give a description of the CM value
        \[
            \Phi \roundbr[\big]{Z(U), f}
        \]
        of $\Phi(z, h, f)$ at $Z(U)$.
        This will be achieved in \Cref{thm:step-3}.
    \end{goal}

    \subsection*{Step 1: Rewrite the CM value as an integral}

    \begin{lemma} \label{lem:step-1}
        We have
        \[
            \Phi \roundbr[\big]{Z(U), f} = \frac{2}{\vol(K_T)} \int_{\SO(U)(\QQ) \backslash \SO(U)(\finadeles)} \Phi(z_U^{\pm}, h, f) \, \dd h.
        \]
    \end{lemma}

    \begin{proof}
        The function
        \[
            T(\finadeles) \to \CC, \qquad h \mapsto \Phi(z_U^{\pm}, h, f)
        \]
        is invariant under $T(\QQ)$, $K_T$ and $\finadeles^{\times}$.
        We thus have
        \begin{align*}
            \Phi \roundbr[\big]{Z(U), f} &= \frac{4}{w_{K, T}} \sum_{h \in T(\QQ) \backslash T(\finadeles) / K_T} \Phi(z_U^{\pm}, h, f) \\
            &= \frac{4}{w_{K, T}} \cdot \frac{1}{\vol \roundbr[\big]{(T(\QQ) \cap K_T) \backslash K_T}} \int_{T(\QQ) \backslash T(\finadeles)} \Phi(z_U^{\pm}, h, f) \, \dd h \\
            &= \frac{2}{\vol(K_T)} \int_{\SO(U)(\QQ) \backslash \SO(U)(\finadeles)} \Phi(z_U^{\pm}, h, f) \, \dd h. \qedhere
        \end{align*}
    \end{proof}

    \begin{remark}
        Using the same method as in the proof of \Cref{lem:step-1} we see that
        \[
            \deg \roundbr[\big]{Z(U)} = \frac{4}{w_{K, T}} \# \roundbr[\big]{T(\QQ) \backslash T(\finadeles) / K_T} = \frac{4}{\vol(K_T)}.
        \]
    \end{remark}

    \subsection*{Step 2: Write $\theta_L(\tau, z_U^{\pm}, h)$ as a product and apply the Siegel-Weil formula}

    \begin{definition}
        We set
        \[
            \theta_P(\tau) \coloneqq \sum_{\mu \in P'/P} \sum_{m \in \QQ_{\geq 0}} r_P(m, \mu) \cdot q^m \cdot \phi_{\mu},
        \]
        where
        \[
            r_P(m, \mu) \coloneqq \# \set[\big]{x \in \mu + P}{Q(x) = m}.
        \]
        For $h \in T(\finadeles)$ we also set
        \[
            \theta_N(\tau, h) \coloneqq v \sum_{\mu \in N'/N} \sum_{m \in \QQ_{\geq 0}} r_N(m, \mu, h) \cdot \overline{q}^m \cdot \phi_{\mu},
        \]
        where
        \[
            r_N(m, \mu, h) \coloneqq \# \set[\big]{x \in h \cdot \roundbr[\big]{\mu + N(\Zhat)} \cap U}{Q(x) = -m}.
        \]
    \end{definition}

    \begin{lemma}
        For $h \in T(\finadeles)$ we have
        \[
            \theta_L(\tau, z_U^{\pm}, h) = \theta_P(\tau) \otimes \theta_N(\tau, h).
        \]
    \end{lemma}

    \begin{proof}
        This follows immediately from the decomposition $L = P \oplus N$.
    \end{proof}

    \begin{proposition}[{\cite[Proposition 2.2]{bruinier-yang}}, Simon's talk] \label{prop:siegel-weil}
        We have
        \[
            \int_{\SO(U)(\QQ) \backslash \SO(U)(\finadeles)} \theta_N(\tau, h) \, \dd h = E_N(\tau, 0; -1).
        \]
        Here we use the notation $E_N(\tau, s; \ell)$ (for $\ell \in \ZZ$) for the $\calS_N$-valued \emph{Eisenstein series} of weight $\ell$ that was introduced in Simon's talk.
    \end{proposition}

    \begin{lemma} \label{lem:step-2}
        Write
        \[
            A_0 \coloneqq \CT \roundbr[\Big]{\anglebr[\big]{f^+(\tau), \theta_P(\tau) \otimes \phi_0}}
        \]
        for the constant term in the $q$-expansion of the function $\anglebr{f^+(\tau), \theta_P(\tau) \otimes \phi_0}$.
        For $T \in \RR_{> 0}$ set
        \[
            I_T \coloneqq \int_{\calF_T} \anglebr[\big]{f(\tau), \theta_P(\tau) \otimes E_N(\tau, 0; -1)} \, \dd \mu(\tau).
        \]
        Then we have
        \[
            \Phi \roundbr[\big]{Z(U), f} = \frac{2}{\vol(K_T)} \lim_{T \to \infty} \roundbr[\big]{I_T - 2 A_0 \log(T)}.
        \]
    \end{lemma}

    \begin{proof}
        One first shows that for $h \in T(\finadeles)$ we have the identity
        \[
            \Phi(z_U^{\pm}, h, f) = \lim_{T \to \infty} \roundbr[\Bigg]{\int_{\calF_T} \anglebr[\big]{f(\tau), \theta_P(\tau) \otimes \theta_N(\tau, h)} \, \dd \mu(\tau) - A_0 \log(T)},
        \]
        see \cite[Lemma 4.6]{bruinier-yang}.
        Using \Cref{lem:step-1} and \Cref{prop:siegel-weil} we then obtain
        \begin{align*}
            \Phi \roundbr[\big]{Z(U), f} &= \frac{2}{\vol(K_T)} \int_{\SO(U)(\QQ) \backslash \SO(U)(\finadeles)} \Phi(z_U^{\pm}, h, f) \, \dd h \\
            &= \frac{2}{\vol(K_T)} \lim_{T \to \infty} \roundbr[\Bigg]{\int_{\calF_T} \anglebr[\Big]{f(\tau), \theta_P(\tau) \otimes \int_{\SO(U)(\QQ) \backslash \SO(U)(\finadeles)} \theta_N(\tau, h) \, \dd h} \, \dd \mu(\tau) - 2 A_0 \log(T)} \\
            &= \frac{2}{\vol(K_T)} \lim_{T \to \infty} \roundbr[\Bigg]{\int_{\calF_T} \anglebr[\big]{f(\tau), \theta_P(\tau) \otimes E_N(\tau, 0; -1)} \, \dd \mu(\tau) - 2 A_0 \log(T)}. \qedhere
        \end{align*}
    \end{proof}

    \subsection*{Step 3: Apply Stokes to the integral $I_T$ and manipulate terms}

    \begin{lemma}[{\cite[Lemma 2.3]{bruinier-yang}}] \label{lem:step-3-eisenstein-identity}
        We have
        \[
            -2 \overline{\partial} \roundbr[\big]{E'_N(\tau, 0; 1) \; \dd \tau} = E_N(\tau, 0; -1) \, \dd \mu(\tau).
        \]
    \end{lemma}

    \begin{definition}[Simon's talk] \label{def:kappa-calE}
        Write
        \[
            E'_N(\tau, 0; 1) = \sum_{\mu \in N'/N} \sum_{m \in \QQ} b_{\mu}(m, v) q^m \phi_{\mu}
        \]
        and set
        \[
            \kappa(m, \mu) \coloneqq
            \begin{cases}
                \lim_{v \to \infty} b_{\mu}(m, v),
                &\text{if $m \neq 0$ or $\mu \neq 0$,}
                \\
                \lim_{v \to \infty} b_0(0, v) - \log(v),
                &\text{if $m = 0$ and $\mu = 0$}
            \end{cases}
        \]
        for $\mu \in N'/N$ and $m \in \QQ_{\geq 0}$.
        Then we define
        \[
            \calE_N(\tau) = \sum_{\mu \in N'/N} \sum_{m \in \QQ_{\geq 0}} \kappa(m, \mu) q^m \phi_{\mu}.
        \]
    \end{definition}

    \begin{remark}
        The derivative $E'_N(\tau, 0; 1)$ is a harmonic weak Maass form of weight $1$ and $\calE_N(\tau)$ is precisely its holomorphic part.
    \end{remark}

    \begin{definition} \label{def:l-function}
        For $g \in S_{1 + n/2, \rho_L}$ we define the associated \emph{$L$-function} $L(g, U, s)$ by
        \[
            L(g, U, s) \coloneqq \roundbr[\big]{\theta_P(\tau) \otimes E_N(\tau, s; 1), g(\tau)}_{\Pet} = \int_{\calF} \anglebr[\Big]{\theta_P(\tau) \otimes E_N(\tau, s; 1), \overline{g(\tau)}} \cdot v^{1 + n/2} \, \dd \mu(\tau).
        \]

    \end{definition}

    \begin{theorem} \label{thm:step-3}
        We have
        \[
            \Phi \roundbr[\big]{Z(U), f} = \frac{4}{\vol(K_T)} \roundbr[\Big]{\CT \roundbr[\Big]{\anglebr[\big]{f^+(\tau), \theta_P(\tau) \otimes \calE_N(\tau)}} - L'\roundbr[\big]{\xi(f), U, 0}}.
        \]
        Here $\xi \colon H_{1 - n/2, \overline{\rho}_L} \to S_{1 + n/2, \rho_L}$ is the semilinear differential operator that was introduced in Rebekka's talk, so that
        \[
            \xi(f)(\tau) = 2 i v^{1 - n/2} \overline{\frac{\partial}{\partial \overline{\tau}} f(\tau)}.
        \]
    \end{theorem}

    \begin{proof}
        Using \Cref{lem:step-3-eisenstein-identity}, the Leibniz rule and Stokes' theorem we first calculate
        \begin{align*}
            I_T &= - 2 \int_{\calF_T} \anglebr[\Big]{f(\tau), \theta_P(\tau) \otimes \overline{\partial} \roundbr[\big]{E'_N(\tau, 0; 1) \, \dd \tau}} \\
            &= -2 \int_{\calF_T} d \anglebr[\big]{f(\tau), \theta_P(\tau) \otimes E'_N(\tau, 0; 1) \, \dd \tau} + 2 \int_{\calF_T} \anglebr[\Big]{\roundbr[\big]{\overline{\partial} f}(\tau), \theta_P(\tau) \otimes E'_N(\tau, 0; 1) \, \dd \tau} \\
            &= 2 \int_{iT - 1/2}^{iT + 1/2} \anglebr[\big]{f(\tau), \theta_P(\tau) \otimes E'_N(\tau, 0; 1)} \, \dd \tau - 2 \int_{\calF_T} \anglebr[\Big]{\overline{\xi(f)(\tau)}, \theta_P(\tau) \otimes E'_N(\tau, 0; 1)} v^{1 + n/2} \, \dd \mu(\tau).
        \end{align*}
        Combining this with the formula from \Cref{lem:step-2} and plugging in \Cref{def:l-function} we obtain
        \[
            \Phi \roundbr[\big]{Z(U), f} = \frac{4}{\vol(K_T)} \roundbr[\Bigg]{\lim_{T \to \infty} \roundbr[\Bigg]{\int_{iT - 1/2}^{iT + 1/2} \anglebr[\big]{f(\tau), \theta_P(\tau) \otimes E'_N(\tau, 0; 1)} \, \dd \tau - A_0 \log(T)} - L'\roundbr[\big]{\xi(f), U, 0}}.
        \]
        As $f^-(\tau)$ is rapidly decreasing for $v \to \infty$ we may replace $f(\tau)$ by $f^+(\tau)$ in the remaining integral.
        Using \Cref{def:kappa-calE} and writing $A_0$ as an integral we then obtain
        \begin{align*}
            &\lim_{T \to \infty} \roundbr[\Bigg]{\int_{iT - 1/2}^{iT + 1/2} \anglebr[\big]{f^+(\tau), \theta_P(\tau) \otimes E'_N(\tau, 0; 1)} \, \dd \tau - A_0 \log(T)} \\
            = &\lim_{T \to \infty}\int_{iT - 1/2}^{iT + 1/2} \anglebr[\Bigg]{f^+(\tau), \theta_P(\tau) \otimes \sum_{\mu \in N'/N} \sum_{m \in \QQ_{\geq 0}} \roundbr[\big]{b_{\mu}(m, v) - \delta_{\mu, 0} \, \delta_{m, 0} \log(v)} q^m \phi_{\mu}} \, \dd \tau \\
            = &\CT \roundbr[\Big]{\anglebr[\big]{f^+(\tau), \theta_P(\tau) \otimes \calE_N(\tau)}}.
        \end{align*}
        This finishes the proof.
    \end{proof}

    \printbibliography
\end{document}
# ws2023-cm-values-green-functions

These are notes for a talk I am giving in the Oberseminar in Bielefeld on the Gross-Zagier formula via Borcherds' lifts (organized by Paul Kiefer).
A compiled version can be found [here](https://makoba.gitlab.io/ws2023-cm-values-green-functions/cm-values.pdf).